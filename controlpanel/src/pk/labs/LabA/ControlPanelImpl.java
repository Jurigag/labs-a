package pk.labs.LabA;

import pk.labs.LabA.Contracts.ControlPanel;
import pk.labs.LabA.Contracts.UkladWejsciowy;

import javax.swing.*;

/**
 * Created by Wojtek on 2014-12-27.
 */
public class ControlPanelImpl implements ControlPanel {
    private JPanel panel;
    private UkladWejsciowy uklad;

    public ControlPanelImpl(UkladWejsciowy uklad)
    {
        this.panel = new JPanel();
        this.uklad=uklad;
    }
    @Override
    public JPanel getPanel() {
        return this.panel;
    }

    public UkladWejsciowy getUklad()
    {
        return this.uklad;
    }
}
