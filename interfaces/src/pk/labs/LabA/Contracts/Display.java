package pk.labs.LabA.Contracts;

import javax.swing.JPanel;

public interface Display {
    JPanel getPanel();
    void setText(String text);
}