package pk.labs.LabA;

import pk.labs.LabA.Contracts.UkladWejsciowy;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = DisplayImpl.class.getName();
    public static String controlPanelImplClassName = ControlPanelImpl.class.getName();

    public static String mainComponentSpecClassName = UkladWejsciowy.class.getName();
    public static String mainComponentImplClassName = UkladWejsciowyImpl.class.getName();
    // endregion

    // region P2
    public static String mainComponentBeanName = "main";
    public static String mainFrameBeanName = "mainFrame";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "sillyBean";
    // endregion
}
