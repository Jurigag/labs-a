package pk.labs.LabA;

import pk.labs.LabA.Contracts.Display;
import pk.labs.LabA.Contracts.UkladWejsciowy;

/**
 * Created by Wojtek on 2014-12-27.
 */
public class UkladWejsciowyImpl implements UkladWejsciowy {
    private Display display;
    public UkladWejsciowyImpl(Display d)
    {
        this.display=d;
    }

    public Display getDisplay()
    {
        return this.display;
    }

    public void setDisplay(Display d)
    {
        this.display=d;
    }

    @Override
    public void Dzialam() {
        display.setText("działam");
    }
}
