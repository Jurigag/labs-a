//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package pk.labs.LabA;

import pk.labs.LabA.Contracts.Display;

import javax.swing.*;

public class DisplayImpl implements Display {
    private String text;
    private JPanel panel = new JPanel();

    public DisplayImpl() {
    }

    @Override
    public JPanel getPanel() {
        return this.panel;
    }

    @Override
    public void setText(String s) {
        this.text = s;
    }
}